/* eslint-disable @next/next/no-img-element */
import GroupIcon from "@mui/icons-material/Group";
import Logout from "@mui/icons-material/Logout";
import NotificationsActiveIcon from "@mui/icons-material/NotificationsActive";
import Avatar from "@mui/material/Avatar";
import FormControl from "@mui/material/FormControl";
import IconButton from "@mui/material/IconButton";
import InputLabel from "@mui/material/InputLabel";
import ListItemIcon from "@mui/material/ListItemIcon";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Pagination from "@mui/material/Pagination";
import Paper from "@mui/material/Paper";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import Stack from "@mui/material/Stack";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/system";
import { signOut, useSession } from "next-auth/react";
import Image from "next/image";
import { useRouter } from "next/router";
import React from "react";
import logo from "../../public/logo.png";

var moment = require("moment");

const ListUser = ({ data }: any) => {
  const { data: session } = useSession();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const router = useRouter();

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },

    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },

    "&:last-child td, &:last-child th": {
      border: 0,
    },
  }));

  function createData(
    owner: string,
    text: number,
    date: number,
    like: number,
    tag: number,
    id: string
  ) {
    return { owner, text, date, like, tag, id };
  }

  const [age, setAge] = React.useState(0);

  const rows =
    age === 10
      ? data.data
          .map((item: any) => {
            return createData(
              item.owner,
              item.text,
              item.publishDate,
              item.likes,
              item.tags,
              item.id
            );
          })
          .sort((a: any, b: any) =>
            a.owner.lastName > b.owner.lastName ? 1 : -1
          )
      : age === 20
      ? data.data
          .map((item: any) => {
            return createData(
              item.owner,
              item.text,
              item.publishDate,
              item.likes,
              item.tags,
              item.id
            );
          })
          .sort((a: any, b: any) =>
            a.owner.lastName > b.owner.lastName ? 1 : -1
          )
          .reverse()
      : data.data.map((item: any) => {
          return createData(
            item.owner,
            item.text,
            item.publishDate,
            item.likes,
            item.tags,
            item.id
          );
        });

  const [page, setPage] = React.useState(1);

  React.useEffect(() => {
    if (!session) {
      router.push("/");
    }
  }, [session]);

  const handleChange = (event: SelectChangeEvent) => {
    setAge(event.target.value as any);
  };

  return (
    <Box sx={{ display: "flex" }}>
      <Box
        sx={{
          width: "255px",
          height: "1200px",
          background: "#363740",
          position: "fixed",
        }}
      >
        <Box
          sx={{
            display: "flex",
            padding: "30px",
            alignItems: "center",
            gap: "10px",
          }}
        >
          <Image src={logo} alt="logo" />
          <Typography
            sx={{
              fontSize: "19px",
              color: "#A4A6B3",
              letterSpacing: "0.4px",
              fontWeight: 700,
              opacity: 0.7,
            }}
          >
            Dashboard Kit
          </Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            gap: "20px",
            padding: "30px",
            background: "#9fa2b412",
            cursor: "pointer",
            borderLeft: "3px solid #fff",
          }}
        >
          <GroupIcon sx={{ color: "#DDE2FF" }} />
          <Typography sx={{ color: "#DDE2FF" }}>User management</Typography>
        </Box>
      </Box>
      <Box
        sx={{
          padding: "30px",
          flexGrow: 1,
          background: "#F7F8FC",
          marginLeft: "255px",
        }}
      >
        <Box
          sx={{ display: "flex", alignItems: "center", marginBottom: "70px" }}
        >
          <Typography sx={{ fontSize: "24px", fontWeight: 600, flexGrow: 1 }}>
            User
          </Typography>
          <Box sx={{ display: "flex", gap: "20px", alignItems: "center" }}>
            <NotificationsActiveIcon sx={{ color: "#abb5ef" }} />
            <Box
              sx={{ width: "1px", height: "30px", background: "#DFE0EB" }}
            ></Box>
            <Typography>{session?.user?.name}</Typography>
            <>
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  textAlign: "center",
                }}
              >
                <Tooltip title="Account settings">
                  <IconButton
                    onClick={handleClick}
                    size="small"
                    aria-controls={open ? "account-menu" : undefined}
                    aria-haspopup="true"
                    aria-expanded={open ? "true" : undefined}
                  >
                    <Avatar
                      sx={{ width: 44, height: 44 }}
                      src={session?.user?.image as any}
                      alt="avatar"
                    />
                  </IconButton>
                </Tooltip>
              </Box>
              <Menu
                anchorEl={anchorEl}
                id="account-menu"
                open={open}
                onClose={handleClose}
                onClick={handleClose}
                PaperProps={{
                  elevation: 0,
                  sx: {
                    overflow: "visible",
                    filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
                    mt: 1.5,
                    "& .MuiAvatar-root": {
                      width: 44,
                      height: 44,
                      ml: -0.5,
                      mr: 1,
                    },
                    "&:before": {
                      content: '""',
                      display: "block",
                      position: "absolute",
                      top: 0,
                      right: 14,
                      width: 10,
                      height: 10,
                      bgcolor: "background.paper",
                      transform: "translateY(-50%) rotate(45deg)",
                      zIndex: 0,
                    },
                  },
                }}
                transformOrigin={{ horizontal: "right", vertical: "top" }}
                anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
              >
                <MenuItem
                  onClick={() => {
                    signOut();
                  }}
                >
                  <ListItemIcon>
                    <Logout fontSize="small" />
                  </ListItemIcon>
                  Logout
                </MenuItem>
              </Menu>
            </>
          </Box>
        </Box>
        <Box sx={{ marginBottom: "20px" }}>
          <FormControl>
            <InputLabel id="demo-simple-select-label">Name</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={age as any}
              label="Age"
              onChange={handleChange}
            >
              <MenuItem value={0}>Sort</MenuItem>
              <MenuItem value={10}>A -&gt; Z</MenuItem>
              <MenuItem value={20}>Z -&gt; A</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <Box>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>Owner</StyledTableCell>
                  <StyledTableCell align="left">Text</StyledTableCell>
                  <StyledTableCell align="left">Date</StyledTableCell>
                  <StyledTableCell align="left">Likes</StyledTableCell>
                  <StyledTableCell align="right">Tags</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {page === 1
                  ? rows.slice(0, 12).map((row: any) => (
                      <StyledTableRow
                        key={row.id}
                        sx={{
                          cursor: "pointer",
                        }}
                        onClick={() => {
                          router.push(`/listUser/post/${row.id}`);
                        }}
                      >
                        <StyledTableCell component="th" scope="row">
                          <Box
                            sx={{
                              display: "flex",
                              alignItems: "center",
                              gap: "10px",
                            }}
                          >
                            <img
                              src={row.owner.picture as any}
                              alt="avatar"
                              style={{
                                width: "44px",
                                height: "44px",
                                borderRadius: "50%",
                              }}
                            />
                            {row.owner.lastName + " " + row.owner.firstName}
                          </Box>
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {row.text}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {moment(row.date).format("MMMM Do YYYY")},
                          {moment(row.date).format(" h:mm:ss a")}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {row.like}
                        </StyledTableCell>
                        <StyledTableCell align="right">
                          {row.tag.map((item: any, index: any) => {
                            return <>{item}, </>;
                          })}
                        </StyledTableCell>
                      </StyledTableRow>
                    ))
                  : rows.slice(12, 24).map((row: any) => (
                      <StyledTableRow
                        key={row.id}
                        sx={{
                          cursor: "pointer",
                        }}
                        onClick={() => {
                          router.push(`/listUser/post/${row.id}`);
                        }}
                      >
                        <StyledTableCell component="th" scope="row">
                          <Box
                            sx={{
                              display: "flex",
                              alignItems: "center",
                              gap: "10px",
                            }}
                          >
                            <img
                              src={row.owner.picture as any}
                              alt="avatar"
                              style={{
                                width: "44px",
                                height: "44px",
                                borderRadius: "50%",
                              }}
                            />
                            {row.owner.lastName + " " + row.owner.firstName}
                          </Box>
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {row.text}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {moment(row.date).format("MMMM Do YYYY")},
                          {moment(row.date).format(" h:mm:ss a")}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                          {row.like}
                        </StyledTableCell>
                        <StyledTableCell align="right">
                          {row.tag.map((item: any, index: any) => {
                            return <>{item}, </>;
                          })}
                        </StyledTableCell>
                      </StyledTableRow>
                    ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Box>
        <Box
          sx={{ display: "flex", justifyContent: "center", marginTop: "20px" }}
        >
          <Stack spacing={2}>
            <Pagination
              count={rows.length / 12}
              variant="outlined"
              shape="rounded"
              page={page}
              onChange={(e, p) => {
                setPage(p);
              }}
            />
          </Stack>
        </Box>
      </Box>
    </Box>
  );
};

export default ListUser;

export async function getStaticProps(context: any) {
  const response = await fetch("https://dummyapi.io/data/v1/post?limit=24", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "app-id": "62ab4986edcbc27ee9d36b41",
    },
  });

  const data = await response.json();

  return {
    props: { data },
  };
}
