/* eslint-disable @next/next/no-img-element */
import ChatIcon from "@mui/icons-material/Chat";
import FavoriteIcon from "@mui/icons-material/Favorite";
import ShareIcon from "@mui/icons-material/Share";
import TagIcon from "@mui/icons-material/Tag";
import Avatar from "@mui/material/Avatar";
import Typography from "@mui/material/Typography";
import { Box } from "@mui/system";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import React from "react";
import styles from "../../../styles/Signin.module.css";
import Button from "@mui/material/Button";

var moment = require("moment");

const Post = ({ dataDetail }: any) => {
  const { data: session } = useSession();
  const router = useRouter();

  React.useEffect(() => {
    if (!session) {
      router.push("/");
    }
  }, [session]);

  return (
    <>
      <Box className={styles.backgroundDetail}></Box>
      <Box sx={{ position: "absolute", top: "20px", left: "20px" }}>
        <Button
          variant="contained"
          sx={{ backgroundColor: "rgba(0,0,0, 0.8)" }}
          onClick={() => {
            router.push("/listUser");
          }}
        >
          Back to List
        </Button>
      </Box>
      <Box
        sx={{
          position: "absolute",
          width: "700px",
          height: "650px",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          zIndex: "2",
          padding: "30px",
          backgroundColor: "rgba(0,0,0, 0.4)",
          borderRadius: "40px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            gap: "20px",
            alignItems: "center",
            marginBottom: "20px",
          }}
        >
          <Avatar
            sx={{ width: "60px", height: "60px", cursor: "pointer" }}
            aria-label="recipe"
            src={dataDetail.owner.picture}
          />
          <Box>
            <Typography
              sx={{ fontSize: "18px", color: "#fff", fontWeight: 700 }}
            >
              {dataDetail.owner.lastName + " " + dataDetail.owner.firstName}
            </Typography>
            <Typography
              sx={{
                fontSize: "12px",
                color: "#fff",
                fontWeight: 500,
                opacity: "0.8",
              }}
            >
              {moment(dataDetail.publishDate).format("MMMM Do YYYY")},
              {moment(dataDetail.publishDate).format(" h:mm:ss a")}
            </Typography>
          </Box>
        </Box>
        <Box>
          <Typography
            sx={{
              fontSize: "16px",
              color: "#fff",
              fontWeight: 600,
            }}
          >
            {dataDetail.text}
          </Typography>
          <Typography
            sx={{
              fontSize: "14px",
              color: "#fff",
              fontWeight: 500,
              display: "flex",
              alignItems: "center",
            }}
          >
            <TagIcon sx={{ color: "#73add1", fontSize: "15px" }} /> Tags:{" "}
            {dataDetail.tags.map((item: any, index: any) => {
              return (
                <Box
                  key={index}
                  sx={{
                    padding: "2px 8px",
                    background: "#333",
                    borderRadius: "10px",
                    margin: "10px 3px",
                    fontSize: "13px",
                    cursor: "pointer",
                  }}
                >
                  {item}
                </Box>
              );
            })}
          </Typography>
        </Box>
        <Box sx={{ height: "63%", marginTop: "10px" }}>
          <img
            src={dataDetail.image}
            alt="img"
            style={{
              objectFit: "cover",
              width: "100%",
              height: "100%",
              borderRadius: "15px",
              cursor: "pointer",
            }}
          />
        </Box>
        <Box sx={{ margin: "12px 0 6px 0" }}>
          <Box sx={{ display: "flex", alignItems: "center", gap: "2px" }}>
            <FavoriteIcon
              style={{ color: "#cf0e0e", fontSize: "30px", cursor: "pointer" }}
            />
            <ChatIcon
              style={{
                color: "#fff",
                fontSize: "25px",
                margin: "0 10px",
                cursor: "pointer",
              }}
            />
            <ShareIcon
              style={{ color: "#fff", fontSize: "25px", cursor: "pointer" }}
            />
          </Box>
        </Box>
        <Box>
          <Typography sx={{ fontSize: "14px", color: "#fff", fontWeight: 400 }}>
            {dataDetail.likes} Like
          </Typography>
        </Box>
      </Box>
    </>
  );
};

export default Post;

export async function getStaticPaths() {
  const res = await fetch("https://dummyapi.io/data/v1/post?limit=24", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "app-id": "62ab4986edcbc27ee9d36b41",
    },
  });
  const data = await res.json();

  const param = data.data.map((item: any) => {
    return { params: { postId: item.id } };
  });

  return {
    paths: param,
    fallback: false,
  };
}

export async function getStaticProps({ params }: any) {
  const res = await fetch(`https://dummyapi.io/data/v1/post/${params.postId}`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "app-id": "62ab4986edcbc27ee9d36b41",
    },
  });

  const dataDetail = await res.json();

  return {
    props: {
      dataDetail,
    },
  };
}
