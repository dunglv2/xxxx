import type { NextPage } from "next";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import React from "react";

const Home: NextPage = () => {
  const { data: session } = useSession();

  const router = useRouter();

  React.useEffect(() => {
    router.push("/signin");
  }, []);

  return <></>;
};

export default Home;
