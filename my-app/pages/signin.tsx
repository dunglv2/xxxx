import {
  getProviders,
  signIn,
  getSession,
  getCsrfToken,
  useSession,
} from "next-auth/react";
import styles from "../styles/Signin.module.css";
import { Box } from "@mui/system";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import React from "react";
import { useRouter } from "next/router";

interface Provider {
  callbackUrl: string;
  id: string;
  name: string;
  signinUrl: string;
  type: string;
}

function Signin({ providers }: any) {
  const { data: session } = useSession();
  const router = useRouter();

  React.useEffect(() => {
    if (session) {
      router.push("/listUser");
    }
  }, [session]);

  return (
    <Box className={styles.backgroundSignin}>
      <Box
        sx={{
          padding: "30px",
        }}
      >
        <Box
          sx={{
            width: "200px",
            height: "50px",
            fontWeight: "800",
            letterSpacing: "2px",
            fontFamily: "fantasy",
            fontSize: "30px",
          }}
        >
          LE DUNG
        </Box>
        <Box
          sx={{
            display: "flex",
            width: "100%",
            justifyContent: "flex-end",
            marginTop: "100px",
          }}
        >
          <Box
            sx={{
              width: "539px",
              height: "300px",
              background: "#ffffffa1",
              borderRadius: "40px",
              padding: "40px",
            }}
          >
            <Typography
              sx={{
                fontSize: "21px",
                fontWeight: 700,
              }}
            >
              Welcome to{" "}
              <span style={{ color: "#779341" }}>User management</span>
            </Typography>
            <Typography
              sx={{
                fontSize: "55px",
                fontWeight: 700,
                marginBottom: "42px",
              }}
            >
              Sign in
            </Typography>
            {Object.values(providers).map((provider: any) => {
              return (
                <div key={provider.name}>
                  <Button
                    sx={{
                      width: "100%",
                      height: "44px",
                      background: "#E9F1FF",
                      borderRadius: "9px",
                    }}
                    onClick={() => {
                      signIn(provider.id);
                    }}
                  >
                    Sign in with {provider.name}
                  </Button>
                </div>
              );
            })}
          </Box>
        </Box>
      </Box>
    </Box>
  );
}

export default Signin;

export async function getServerSideProps() {
  return {
    props: {
      providers: await getProviders(),
    },
  };
}
